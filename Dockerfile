FROM python:3.5-alpine3.8

# TODO Set metadata for who maintains this image
LABEL maintainer="John Brown"
LABEL version="1.0"


COPY * /app/
RUN pip3 install Flask
RUN apk add curl
EXPOSE 8080

#TODO Set default values for env variables
ENV ENVIRONMENT=DEV
ENV DISPLAY_FONT=arial
ENV DISPLAY_COLOR=red


#TODO *bonus* add a health check that tells docker the app is running properly
HEALTHCHECK --interval=1m --timeout=3s \
  CMD curl -f http://localhost:8090/ || exit 1


# TODO have the app run as a non-root user
USER 1004

CMD python /app/app.py