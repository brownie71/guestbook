import os
from flask import Flask, redirect, request, url_for
import logging


logging.basicConfig(format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)
# has_config = config.read('vars.ini')
# stream=sys.stdout
signatures = []

app = Flask(__name__)

# configurations
font = os.environ['DISPLAY_FONT'] or 'DEV'
font_color = os.environ['DISPLAY_COLOR'] or 'arial'
environment = os.environ['ENVIRONMENT'] or 'red'

@app.route('/', methods=['GET'])

def index():

    html = """
    Signatures: <br />
    <font face="%(font)s" color="%(color)s">
        %(messages)s
    </font>

    <br /> <br />
    <form action="/signatures" method="post">
        Sign the Guestbook: <input type="text" name="message"><br>
        <input type="submit" value="Sign">
    </form>

    <br />
    <br />
    Debug Info: <br />

    ENVIRONMENT is %(environment)s

    """

    messages_html = "<br />".join(signatures)

    return html % {"font": font, "color": font_color, "messages": messages_html, "environment": environment}



@app.route('/signatures', methods=['POST'])

def write():

    message = request.form.get('message')

    signatures.append(message)



    return redirect(url_for('index'))



if __name__ == "__main__":

    app.run(host='0.0.0.0', port=8090)

